const env = 'pro'

let envConfig = {
	getEnv() {
		return env;
	},
	getParams() {
		return this[env];
	},
	sit: {
		serverUrl: 'www.sit.com'
	},
	uat: {
		serverUrl: 'www.uat.com'
	},
	trl: {
		serverUrl: 'www.trl.com'
	},
	pro: {
		serverUrl: 'https://zyfun-byljfl2.by.gov.cn/weixin-volunteer/api'
	}
};

export default envConfig
