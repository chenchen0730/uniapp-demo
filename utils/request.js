import envConfig from '../envConfig.js'

const env = envConfig.getEnv();
const envConfigData = envConfig.getParams();

export default {
	// 封装request
	request({
		api,
		method,
		data
	}) {
		var serverUrl = `${envConfigData.serverUrl}/${api}`
		uni.showLoading({
			title: '加载中',
			mask: true
		})

		return new Promise((resolve, reject) => {
			uni.request({
				url: serverUrl,
				method: method.toUpperCase(),
				data,
				header: {
					'content-type': 'application/json;charset=utf-8'
				},

				success: res => {
					uni.hideLoading()
					if (res.data.code !== 200) {
						return uni.showToast({
							title: "数据获取失败！",
							icon: 'error'
						})
					}
					resolve(res.data.data)
				},

				fail: err => {
					uni.hideLoading()
					return uni.showToast({
						title: "数据获取失败！",
						icon: 'error'
					})
				}
			});

		})
	},

	//get方法
	get(options) {
		options.method = "get"
		return this.request(options)
	},

	//post方法
	post(options) {
		options.method = "post"
		return this.request(options)
	}
}
