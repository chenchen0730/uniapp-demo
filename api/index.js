import request from 'utils/request.js'

export const getBanner = (data = {}) => {
	return request.post({
		api: 'login/getBanner',
		data
	})
}
